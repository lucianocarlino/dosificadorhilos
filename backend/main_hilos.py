import multiprocessing
import traceback
from backend.lib.sistema.threads.main_thread import Main_Thread
from backend.lib.sistema.threads.processing_thread import Processing_Thread
from shared.Configuracion import Configuracion

def main_thread(cola_imagen, cola_resultados):
    MainThread = Main_Thread()
    MainThread.proceso(cola_imagen, cola_resultados)

def processing_thread(cola_imagen, cola_resultados):
    ProcessingThread = Processing_Thread()
    ProcessingThread.proceso(cola_imagen, cola_resultados)

try:
    if __name__ == '__main__':
        ColaImagen = multiprocessing.Queue()
        ColaResultados = multiprocessing.Queue()
        NumeroCamaras = Configuracion.getValor("cantidad_camaras")

        MainThread = multiprocessing.Process(target=main_thread, args=(ColaImagen, ColaResultados), name="main_thread")
        ProcessingThread = multiprocessing.Process(target=processing_thread, args=(ColaImagen, ColaResultados), name="processing_thread")

        MainThread.start()
        ProcessingThread.start()

except:
    print('ERROR, main has failed')
    traceback.print_exc()