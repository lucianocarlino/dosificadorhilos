from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from Configuracion import Configuracion
import time
from RaspberryGPIO import RaspberryGPIO
from lib.adquisicion.SensorValle import SensorValle
class Bomba():

  def __init__(self):
      self._puertoVariador = ('/dev/ttyUSB0')  
      self._clienteModbus = ModbusClient(method='rtu', port=self._puertoVariador, timeout=2, baudrate=115200, stopbits=1, bytesize=8, parity="N", strict=False)
      self._velocidadVariadorHertz =0
      self._LeervelocidadVariadorHertz = 0
      self._velocidad = 0
      self._ajusteVelocidadvariador = float(Configuracion.getValor("ajuste_de_velocidad_variador"))
      self._tamañoRegistros=5
      self._estadisticaEscrituraVariador = list(range( self._tamañoRegistros,0))
      self._tamañoRegistroBomba=5
      self._retardoEscrituraBomba = list(range(self._tamañoRegistroBomba,0))
      self._resetListaVariador=True
      self._resetListaDosBomba=True
      self._tiempoAccionamientoReleUno = time.time()
      self._tiempoAccionamientoReleDos = time.time()
      RaspberryGPIO.terminaAcelerarVariadorMesaInspeccion()
      RaspberryGPIO.terminaFrenarVariadorMesaInspeccion()
      self._tamañoRegistroVelocidadMesa=5
      self._tamañoRegistrollenadoAnterior=5
      self._estadisticaEscrituraMesaInspeccion=list(range(self._tamañoRegistroVelocidadMesa,0))
      self._estadisticaLLenadoAnterior=list(range(self._tamañoRegistrollenadoAnterior,0))
      self._resetMesaInspeccion=True
      self._llenadoAnterior=0
      self._coeficiente=0.4
      self._sensor=SensorValle()
      
      for m in range (self._tamañoRegistrollenadoAnterior):
           self._estadisticaLLenadoAnterior.append(0)
      self._tiempoInicioFrena= 0
      self._tiempoFrena=0
      self._tiempoInicioAcelera=0
      self._tiempoAcelera=0
      self._variacionLLenado=0
      self._tiempoCalculoActualizacionVelocidadMesa=0.2
      self._maximaLLenadoReal=33
      self._variableError=1
      print('[Bomba] >>> [Iniciado Correctamente]')
  
  def reconectarVariador(self):
      self._puertoVariador = ('/dev/ttyUSB0')  
      self._clienteModbus = ModbusClient(method='rtu', port=self._puertoVariador, timeout=2, baudrate=115200, stopbits=1, bytesize=8, parity="N", strict=False)
      self._velocidadVariadorHertz =0
      self._LeervelocidadVariadorHertz = 0
      self._velocidad = 70
      self._ajusteVelocidadvariador = float(Configuracion.getValor("ajuste_de_velocidad_variador"))
      self._estadisticaEscrituraVariador =list(range( self._tamañoRegistros)) 
      print('[Bomba] >>> [Iniciado Correctamente]')



  def escribirEnVariador(self, mililitrosPorMinuto):
          mililitroEstadisticoPorMinuto = self.obtenerEstadisticaDeEscritura(mililitrosPorMinuto) 
        # -----RETARDO ESCRITURA-----
          if self._resetListaDosBomba ==True:
            for c in range(self._tamañoRegistroBomba):
              self._retardoEscrituraBomba.append(0)
              self._resetListaDosBomba=False
          del self._retardoEscrituraBomba[0]
          self._retardoEscrituraBomba.append (mililitroEstadisticoPorMinuto) 
          self._valorretardo=self._retardoEscrituraBomba[0]
          self._velocidadVariadorHertz  = ((self._valorretardo* (self._ajusteVelocidadvariador/3600) * 16387)*self._variableError ) 
          print("----VELOCIDAD CALCULO-----",self._velocidadVariadorHertz)
          self._clienteModbus.write_register(0, 6, unit=1)
          if self._velocidadVariadorHertz < 16387:
               self._clienteModbus.write_register(1, int(self._velocidadVariadorHertz), unit=1)
          if self._velocidadVariadorHertz > 16387:
               self._clienteModbus.write_register(1, int(16387), unit=1)
               

          

  def obtenerEstadisticaDeEscritura(self, mililitrosPorMinuto):
       if self._resetListaVariador == True:
         for i in range(self._tamañoRegistros):
           self._estadisticaEscrituraVariador.append(0)
           self._resetListaVariador=False
       del self._estadisticaEscrituraVariador[0]
       self._estadisticaEscrituraVariador.append (mililitrosPorMinuto) #AGREGAR DOSIFICADOR 
       valorEstadisticoDeEscritura = sum (self._estadisticaEscrituraVariador)# ((self._estadisticaEscrituraVariador[1]+self._estadisticaEscrituraVariador[2]+self._estadisticaEscrituraVariador[3]+self._estadisticaEscrituraVariador[4]+self._estadisticaEscrituraVariador[5])/5)
       return valorEstadisticoDeEscritura /  self._tamañoRegistros

  def leerVelocidadenHertz(self):
       try:
          self._LeervelocidadVariadorHertz = (self._clienteModbus.read_holding_registers(6 , 1 , unit=1 )). registers
          for self._velocidad in self._LeervelocidadVariadorHertz:
                 self._LeervelocidadVariadorHertzModbus = float(self._velocidad/float(16384)*60)
                 self._velocidad=self._LeervelocidadVariadorHertzModbus
          return  self._velocidad
       except Exception as identifier:
          self._velocidad=0
          return self._velocidad
  
  def GetVelocidadVariador(self):
       return "{:.2f}".format(float(self._velocidad))
  
  def pararVariador(self):
      self._clienteModbus.write_register(0, 2, unit=1)
      return True
  
  def velocidadMaximavariador(self):
       self._clienteModbus.write_register(0, 6, unit=1)
       self._clienteModbus.write_register(1, 16387 , unit=1)

  def escribirVariadorMesaUno(self,porcentajeLLenado,calcular,setLLenado,tiempoanterior): 
       self._setLLenado = setLLenado 
       if self._resetMesaInspeccion == True:
         for m in range (self._tamañoRegistroVelocidadMesa):
           self._estadisticaEscrituraMesaInspeccion.append(0)
           self._resetMesaInspeccion=False
       self._variacionLLenado= porcentajeLLenado -self._llenadoAnterior
       self._llenadoAnterior=porcentajeLLenado

       del self._estadisticaLLenadoAnterior[0]
       self._estadisticaLLenadoAnterior.append( self._variacionLLenado)
       self._valorEstadisticoLLenadoAnterior= sum(self._estadisticaLLenadoAnterior)
       self._llenadoAnteriorPromedio = self._valorEstadisticoLLenadoAnterior/self._tamañoRegistrollenadoAnterior
      # print("LLENADO ANTERIOR",self._estadisticaLLenadoAnterior)
       
       del self._estadisticaEscrituraMesaInspeccion[0]
       self._estadisticaEscrituraMesaInspeccion.append(porcentajeLLenado)
       self._valorEstadisticoMesa= sum(self._estadisticaEscrituraMesaInspeccion)
       self._porcentajeTotal = self._valorEstadisticoMesa/self._tamañoRegistroVelocidadMesa
       self._variableComparativa= (self._porcentajeTotal*self._coeficiente+(1-self._coeficiente)* self._llenadoAnteriorPromedio*self._tamañoRegistrollenadoAnterior)
       #print("PORCENTAJE TOTAL LLENADO PROMEDIO", porcentajeTotal,self._llenadoAnteriorPromedio)
       #print("PORCENTAJE TOTAL TABLA",self._estadisticaEscrituraMesaInspeccion)
       if calcular==True:
          
          print("VARIABLE COMPARATIVA",self._variableComparativa)
          if (self._variableComparativa <= self._setLLenado * 0.95) :
               self._tiempoInicioFrena=(time.time()) + self._tiempoCalculoActualizacionVelocidadMesa
               self._tiempoFrena= (time.time() + (float(str(tiempoanterior))/self._maximaLLenadoReal *(setLLenado-self._variableComparativa)) )+ self._tiempoCalculoActualizacionVelocidadMesa
               print("TIEMPO ANTERIOR FRENA",tiempoanterior)
               print('----------FRENA MESA -----------',(float(str(tiempoanterior))/self._maximaLLenadoReal *(setLLenado-self._variableComparativa)))
          if self._variableComparativa > self._setLLenado * 1.05 :
               self._tiempoInicioAcelera=(time.time())+self._tiempoCalculoActualizacionVelocidadMesa
               self._tiempoAcelera =( time.time() + (float(str(tiempoanterior)) / self._maximaLLenadoReal * (self._variableComparativa - setLLenado))) + self._tiempoCalculoActualizacionVelocidadMesa
               print("TIEMPO ANTERIOR",tiempoanterior)
               print('----------ACELERA MESA----------',(float(str(tiempoanterior)) / self._maximaLLenadoReal * (self._variableComparativa - setLLenado)))
  def getPorcentajeLlenadoReal(self):
       self.escribirVariadorMesaUno
       self._porcentajeTotal
       return "{0:.0f}".format((self._porcentajeTotal)*3)
  def tiempoiniciofrena(self):
       self.escribirVariadorMesaUno
       return self._tiempoInicioFrena  
  def tiempofrena(self):
       self.escribirVariadorMesaUno
       return self._tiempoFrena
  def tiempoinicioacelera(self):
       self.escribirVariadorMesaUno
       return self._tiempoInicioAcelera
  def tiempoacelera(self):
       self.escribirVariadorMesaUno
       return self._tiempoAcelera

  def frenarMesa(self):
       RaspberryGPIO.frenaVariadorMesaInspeccion()
  def terminarDeFrenar(self):
       RaspberryGPIO.terminaFrenarVariadorMesaInspeccion()
  def acelerarMesa(self):
      RaspberryGPIO.aceleraVariadorMesaInspeccion()
  def terminarAcelerar(self):
        RaspberryGPIO.terminaAcelerarVariadorMesaInspeccion()

  def escribirVariadorMesaDos(self,porcentajeLLenado,calcular,setLLenado):
       self._setLLenado = setLLenado
       if calcular == True:
          if porcentajeLLenado <= self._setLLenado*0.9 :
               self._velocidadMesaDos =  int(self._velocidadMesaDos*0.95)
               if self._velocidadMesaDos <= 6200:
                    self._velocidadMesaDos = 6200
               #print('----------FRENA MESA DOS -----------',self._velocidadMesaDos)
          if porcentajeLLenado  > self._setLLenado * 1.1  : 
                self._velocidadMesaDos =  int(self._velocidadMesaDos*1.01)
                if self._velocidadMesaDos > 16350:
                     self._velocidadMesaDos = 16387
                #print('----------ACELERA MESA DOS ----------',self._velocidadMesaDos)
       self._clienteModbus.write_register(0, 6, unit=3)
       self._clienteModbus.write_register(1,(self._velocidadMesaDos) , unit=3)
 
  #----------velocidad minima version 1.2 = 101
  #----------velocidad maxima version 1.2 = 100
  #----------velocidad minima version 2 = 39
  #----------velocidad maxima version 2 = 44
  #----------obtener numero de version registro 14
  
  def leerVelocidadMaximaVariadorMesaUno(self): # MESA UNO 
       try:
          leerEntradaPotenciometromesauno = (self._clienteModbus.read_holding_registers(44 , 1 , unit=2 )). registers 
          for velocidad in leerEntradaPotenciometromesauno:                             
                 leerEntradaPotenciometromesaunoModbus =  float (velocidad )
                 #print('VELOCIDAD MAXIMA MESA UNO',leerEntradaPotenciometromesaunoModbus)
          return leerEntradaPotenciometromesaunoModbus 
       except Exception as identifier:
         print(identifier)
         return 0
  def leerVelocidadMinimaVariadorMesaUno(self): # MESA UNO 
       try:
          leerEntradaPotenciometromesauno = (self._clienteModbus.read_holding_registers(39 , 1 , unit=2 )). registers 
          for velocidad in leerEntradaPotenciometromesauno:                              
                 leerEntradaPotenciometromesaunoModbus =  int (velocidad )
                 #print('----VELOCIDAD MINIMA MESA UNO----',leerEntradaPotenciometromesaunoModbus)
          return leerEntradaPotenciometromesaunoModbus 
       except Exception as identifier:
         print(identifier)
         return 0
  
  def leerVelocidadMaximaVariadorMesaDos(self): # MESA DOS
       try:   
          leerEntradaPotenciometromesados = (self._clienteModbus.read_holding_registers(44 , 1 , unit=3 )). registers
          for velocidad in leerEntradaPotenciometromesados: 
                 leerEntradaPotenciometromesadosModbus = int (velocidad )
                 #print('VELOCIDAD MAXIMA MESA DOS',leerEntradaPotenciometromesadosModbus)
          return leerEntradaPotenciometromesadosModbus 
       except Exception as identifier:
         print(identifier)
         return 0
  def leerVelocidadMinimaVariadorMesaDos(self): #MESA DOS
       try:
          leerEntradaPotenciometromesados = (self._clienteModbus.read_holding_registers(39 , 1 , unit=3 )). registers
          for velocidad in leerEntradaPotenciometromesados: 
                 leerEntradaPotenciometromesadosModbus = int (velocidad )
                 #print('VELOCIDAD MAINIMA MESA DOS',leerEntradaPotenciometromesadosModbus)
          return leerEntradaPotenciometromesadosModbus 
       except Exception as identifier:
         print(identifier)
         return 0
