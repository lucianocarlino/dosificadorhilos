from lib.adquisicion.SensorValle import SensorValle
from Configuracion import Configuracion
from RaspberryGPIO import RaspberryGPIO
from lib.actuacion.Funcionamiento import  Funcionamiento
class Estadisticas():
  
    def __init__(self,litrosAplicadosDb,toneladasAcumuladasDb):
        self._litrosPortonelada = float(Configuracion.getValor("estadistica.litros_por_tonelada"))
        self._mililitrosPorminutoManual = float(Configuracion.getValor("estadisticas.mililitros_por_minuto_manual"))
        self._kilogramosPorhora = 0
        self._mililitrosPorminuto = 0
        self._tiempoLLenadodeJeringa = 0
        self._toneladasAcumuladas =toneladasAcumuladasDb
        self._litrosAplicados = litrosAplicadosDb
        self._toneladasHora = 0
        self._thislist = [0.0,0.0,0.0,0.0,0.0,0.0]
        self._largoThislist = 5
        self._sumaThislist=0
        self._reset=False
        SensorValle()
        RaspberryGPIO()
        self._tamañoRegistroToneladasHora=10
        self._estadisticaToneladasHora=list(range(self._tamañoRegistroToneladasHora,0))
        for m in range (self._tamañoRegistroToneladasHora):
           self._estadisticaToneladasHora.append(0)
        self._funcionamiento=Funcionamiento()


    
    def setKilogramosValleTotal(self,kilogramoTotalTruta):
        self._kilogramosValleTotal =  kilogramoTotalTruta
    
    def GetKgValle(self):
        self._kilogramosValleTotal
        return "{:.2f}".format(float(self._kilogramosValleTotal )) 
    
    def thislistKgValle(self):
        self._thislist.append (self._kilogramosValleTotal)

    def setTiempoCiclo(self,tiempoCicloSensorValle):
        self._tiempoCiclo =  tiempoCicloSensorValle

    def kilogramosPorhora(self):
        if self._kilogramosValleTotal > 0.0010:
            self._kilogramosPorhora = ( ( self._kilogramosValleTotal * 3600 ) / SensorValle._tiempoCiclo ) 
            return  self._kilogramosPorhora
    
    
    def mililitrosPorminuto(self):
        self._litrosPortonelada = float(Configuracion.getValor("estadistica.litros_por_tonelada"))
        self.kilogramosPorhora()
        self._mililitrosPorminuto = ( (self._litrosPortonelada * self._kilogramosPorhora) / 60 )
        return self._mililitrosPorminuto 
    
    def GetMililitrosPorMinuto(self):
        print ("------MILILITROS MINUTO-----",self._mililitrosPorminuto)
    
    def mililitrosPorminutoManual(self,mililitrosManual):
        self._mililitrosPorminutoManual=mililitrosManual
        return   self._mililitrosPorminutoManual
    
    def GetmililitrosMinutoManual(self):
        return self._mililitrosPorminutoManual

    def tiempoLLenadodeJeringapara60ml(self): #tiempo referencia para calibrar bomba variador 
        self.mililitrosPorminuto()
        if ((self._mililitrosPorminuto > 0 )and  (SensorValle._tiempoCiclo < 0.80)):
            self._tiempoLLenadodeJeringa = 3600 / self._mililitrosPorminuto
        return self._tiempoLLenadodeJeringa 
        
    def toneladasAcumuladas(self):
        self._toneladasAcumuladas =  ( self._toneladasAcumuladas + self._kilogramosValleTotal/1000  ) 
        return "{:.2f}".format(float( self._toneladasAcumuladas))
    
    def GetToneladasAcumuladas(self):
        return "{:.2f}".format(float( self._toneladasAcumuladas))
    
    def litrosAplicados(self):
        self._litrosPortonelada = float(Configuracion.getValor("estadistica.litros_por_tonelada"))
        if RaspberryGPIO.obtenerValorCalibrado()=="AUTOMATICO":
            self._litrosAplicados = ( (self._toneladasAcumuladas * self._litrosPortonelada  )  )
        if RaspberryGPIO.obtenerValorCalibrado()=="MANUAL":
            self.mililitrosPorminutoManual
            self._litrosAplicados=self._litrosAplicados + (self._mililitrosPorminutoManual/1000)
        return "{:.2f}".format(float(self._litrosAplicados )) 
    
    def GetLittrosAplicados(self):
        return "{:.2f}".format(float(self._litrosAplicados ))

    def litrosPortonelada(self):
        self._litrosPortonelada=float(Configuracion.getValor("estadistica.litros_por_tonelada"))
        return self._litrosPortonelada

    def toneladasHora(self):
        self._toneladasHora = ((self._kilogramosValleTotal / SensorValle._tiempoCiclo) *3.6)
        del self._estadisticaToneladasHora[0]
        self._estadisticaToneladasHora.append(self._toneladasHora )
        self._valorEstadisticoToneladasHora= sum(self._estadisticaToneladasHora)
        self._toneladasHoraPromedio = self._valorEstadisticoToneladasHora/self._tamañoRegistroToneladasHora
       # print('-------TONELADAS HORA-----',"{:.3f}".format(float(self._toneladasHora)))
        return "{:.2f}".format(float(self._toneladasHoraPromedio))
    
    def GetToneladasHora(self):
        return "{:.2f}".format(float(self._toneladasHoraPromedio ))

    def ponerValoresEnCero(self):
        self._funcionamiento.GuardarDatosTxtReset()

        self._litrosPortonelada = 0
        self._toneladasAcumuladas = 0
        self._litrosAplicados=0

    