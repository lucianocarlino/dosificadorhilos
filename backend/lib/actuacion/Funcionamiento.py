from datetime import date
from datetime import datetime
import time

class Funcionamiento():

  def __init__(self):
      self._dias = date.today()
      self._hora = datetime.now()
      self._estado="INICIADO"
      self._estadoanteriorrango=2
      self._estadoanterior=list(range( self._estadoanteriorrango,0))
      for m in range (self._estadoanteriorrango):
       self._estadoanterior.append("sin_estado")
      self._estadoanteriorrangocadena=2
      self._estadoanteriorcadena=list(range( self._estadoanteriorrangocadena,0))
      for m in range (self._estadoanteriorrangocadena):
       self._estadoanteriorcadena.append("sin_estado")
      
      print('[Registro de Funcionamiento] >>> [Iniciado Correctamente]')
  
  def fechaHoraActual(self,datos):
      self._meses = ("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
              "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
      self._dias = datos.day
      self._mes = self._meses[datos.month - 1]
      self._año = datos.year
      self._hora = time.strftime("%H:%M:%S")
      self._fechaHora = "{} de {} del {} / hs {}  ".format(self._dias, self._mes, self._año, self._hora)
      return self._fechaHora

  

  def estadoAnterior(self,estadoNuevo,estadocadena):
       self.estadoAnteriorCadena(estadocadena)
       del self._estadoanterior[0]
       self._estadoanterior.append(estadoNuevo)
       self._estadoAcomparar= self._estadoanterior[0]
  
  def estadoAnteriorCadena(self,estadocadena):

       del self._estadoanteriorcadena[0]
       self._estadoanteriorcadena.append(estadocadena)
       self._estadoAcompararcadena= self._estadoanteriorcadena[0]


  def GuardarDatosTxt(self,estadoanterior,cadena,toneladas,litros,litrosportonelada):
       self.estadoAnterior(estadoanterior,cadena)
       
       self.fechaHoraActual(datetime.now())
       self._archivoTxt=r'/home/pi/datos_funcionamiento.txt'
       if self._estadoAcomparar != estadoanterior or self._estadoAcompararcadena != cadena:

          with open (self._archivoTxt,'a') as f:
               f.write( str(self._fechaHora)+  "  " + 'ESTADO: ' + " / " + "  " + str(estadoanterior)+ " / " + "  " + "  " + 'ERRORES: ' + " / " + "  " +str(cadena)+ " / " + "  " + "  " + 'TONELADAS: '+str(toneladas)+ " / " + "  " + "  " + 'LITROS: '+str(litros)+ " / " + "  " + "  " + 'LITROS POR TONELADA: '+str(litrosportonelada)+'\n'+'\n')
               print(estadoanterior,cadena)
  def GuardarDatosTxtReset(self):
       
       self.fechaHoraActual(datetime.now())
       self._archivoTxt=r'/home/pi/datos_funcionamiento.txt'
       with open (self._archivoTxt,'a') as f:
               f.write(str(self._fechaHora)+  "  " + 'ESTADO: ' + " / " + "   " + str("reset")+ " / " + "  " + "  " + 'ERRORES: ' + " / " + "  " +str("reset")+'\n'+'\n')
