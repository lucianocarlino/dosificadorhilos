import traceback
import cv2
import numpy as np

from shared.Configuracion import Configuracion

class Procesamiento_imagen():
    def __init__(self, nombreCamara):
        self._nombreCamara = nombreCamara
        
    @staticmethod
    def calcularMatrizBN(cropped, nombreCamara):
        try:
            imagenPixelesEnBlue, imagenPixelesEnGreen, imagenPixelesEnRed = cv2.split(cropped)
            #TODO De la forma que esta implementado, cada vez que necesite un valor de la db hace un query, deberiamos corregir eso con persistencia
            ajusteFuncionMatricialUno = int(Configuracion.getValor(f"camara.{nombreCamara}.ajuste_uno_funcion_matricial"))
            ajusteFuncionMatricialDos = float(Configuracion.getValor(f"camara.{nombreCamara}.ajuste_dos_funcion_matricial"))
            verdeBajoscolor = int(Configuracion.getValor(f"camara.{nombreCamara}.verdes_bajos_color"))
            imagenTransformadaParaCalculo = (imagenPixelesEnGreen-ajusteFuncionMatricialUno)*ajusteFuncionMatricialDos-(imagenPixelesEnBlue)
            nuevaMatriz = cv2.merge((imagenTransformadaParaCalculo,imagenTransformadaParaCalculo,imagenTransformadaParaCalculo))
            verde_bajos = np.array([0,0,0], dtype=np.uint8)
            verde_altos = np.array([verdeBajoscolor,255,255], dtype=np.uint8)
            mascara = cv2.inRange(nuevaMatriz, verde_bajos,verde_altos) 
            return mascara
        except:
            print('ERROR, calcularKgFruta method has failed')
            traceback.print_exc()

    @staticmethod
    def calcularkilogramosPorValle(mascara, nombreCamara):
        variableCalculopesoPorkg = float(Configuracion.getValor(f"camara.{nombreCamara}.pixel_por_kilogramo"))
        pixelesBlancos = cv2.countNonZero(mascara)
        kilogramoPorvalle = pixelesBlancos * variableCalculopesoPorkg
        return kilogramoPorvalle
    
    @staticmethod
    def calcularLlenado(mascara):
        pixelesBlancos = cv2.countNonZero(mascara)
        llenado = (pixelesBlancos / mascara.size) * 100
        return llenado
