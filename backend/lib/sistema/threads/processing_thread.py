import traceback
from backend.lib.actuacion.procesamiento_imagen import Procesamiento_imagen

class Processing_Thread():
    def __init__(self,):
        pass

    def proceso(self, cola_imagen, cola_resultados):
        while True:
            kilogramoPorValle = 0
            llenado = 0
            try:
                if not cola_imagen.empty():
                    imagenes = cola_imagen.get() #Saco un arreglo de imagenes de la cola
                    for imagen, indice in imagenes:
                        mascara = Procesamiento_imagen.calcularMatrizBN(imagen, f"camara_{indice+1}")
                        kilogramoPorValle += Procesamiento_imagen.calcularKgFruta(mascara, f"camara_{indice+1}")
                        llenado += Procesamiento_imagen.calcularLlenado(mascara)
                    cola_resultados.put(kilogramoPorValle, llenado)

            except:
                print('ERROR, proceso method has failed')
                traceback.print_exc()

