import traceback
import time
from backend.lib.adquisicion.Camara import Camara
from backend.lib.adquisicion.SensorValle import SensorValle
from backend.lib.sistema.Dosificador import Dosificador
from shared.Configuracion import Configuracion
from shared.RaspberryGPIO import RaspberryGPIO
from shared.errores.CintaParadaException import CintaParadaException

class Main_Thread():
    def __init__(self,):

        self.dosificador = Dosificador()
        self.imagenes = []


    def proceso(self, cola_imagen, cola_resultados):
        self.timeAnt = time.perf_counter()
        self.pulso = 0 #TODO implementar que cuente los pulsos
        self.iteracion = 0 #TODO implementar que cuente las vueltas de sistema
        while True:
            try:
                self.timeAct = time.perf_counter()
                self.dosificador.actualizarVelocidadMesas()
                self.dosificador.actualizarDatos()
                if (SensorValle().debeTomarFoto()):
                    self.dosificador._bombaVariador.escribirEnVariadorMesaUno(self.dosificador._porcentajeLLenado, 
                                                                              self.dosificador._calcular,
                                                                              self.dosificador._setLLenado,
                                                                              SensorValle().getTiempocicloAnterior())
                    self.dosificador._calcular = False
                    self.dosificador._pulsosReset += 1
                    self.dosificador._mensajeError = 'MSG_000_SIN_ERROR'
                    for i in range(int(Configuracion.getValor("cantidad_camaras"))):
                        try:
                            imagen = self.dosificador.camaras[i].sacarFoto() #TODO Solo sacar la foto
                            self.imagenes.append([imagen, i])
                        except:
                            print(f"Error sacando foto en camara {i+1}")
                    cola_imagen.put(self.imagenes) #TODO Solo poner en la cola el arreglo de imagenes
                    if not cola_resultados.empty(): #TODO Guardar en la cola de resultados una tupla compuesta por kgvalle y llenado
                        self.dosificador._kgtotal, self.dosificador._porcentajeLLenado = cola_resultados.get() 
                    self.dosificador._calcular = True
                    print("---- KG VALLE ---- ","{:.3f}".format(float(str(self.dosificador._kgtotal))))
                    print("---- LLENADO ---- ","{:.3f}".format(float(str(self.dosificador._porcentajeLLenado))))
                    if ((self.dosificador._kgtotal > 0.08) and (RaspberryGPIO.obtenerValorCalibrado() == "AUTOMATICO")):
                        self.dosificador._kgtotalSend = self.dosificador._kgtotal
                    else:
                        self.dosificador._kgtotalSend = 0.001
                    self.dosificador._estadisticas.setKilogramosValleTotal(self.dosificador._kgtotalSend)
                    self.dosificador._estadisticas.setTiempoCiclo(SensorValle.getTiempoCiclo)
                    self.dosificador.guardarDatosEstadisticos()
                    self.dosificador._intercomunidador.guardarDatosEnDB()

                    if (RaspberryGPIO.obtenerValorCalibrado() == "MANUAL"):
                        print("---- Manual ----")
                        Configuracion.loadConfiguracionFromDatabase()
                        mililitros_por_minuto_manual = float(Configuracion.getValor("estadsiticas.mililitros_por_minuto_manual"))
                        self.dosificador._estadisticas.mililitrosPorminutoManual(mililitros_por_minuto_manual)
                        self.dosificador._bombaVariador.escribirEnVariador(mililitros_por_minuto_manual)
                    elif (RaspberryGPIO.obtenerValorCalibrado() == "AUTOMATICO"):
                        print("---- Automatico ----")
                        if (self.dosificador._kgtotal > 0.08):
                            self.dosificador._kgtotalSend = self.dosificador._kgtotal
                            if (self.dosificador._kgtotal > 0.1):
                                try:
                                    self.dosificador._bombaVariador.escribirEnVariador(self.dosificador._estadisticas.mililitrosPorminuto())
                                except Exception as identifier:
                                    print("No se pudo escribir en bomba,Reconectando comunicacion",identifier)
                                    self.dosificador._mensajeError = 'MSG_002_VARIADOR_DESCONECTADO'
                                    self.dosificador._bombaVariador.reconectarVariador()
                                    time.sleep(0.5)
                        else:
                            self.dosificador._kgtotalSend = 0.001
                        if (self.dosificador._kgtotal < 0.1):
                            self.dosificador._intercomunidador.ponerEnceroDb()
                            try:
                                self.dosificador._bombaVariador.pararVariador()
                            except Exception as identifier:
                                print("No se pudo escribir en bomba,Reconectando comunicacion",identifier)
                                self.dosificador._mensajeError = 'MSG_002_VARIADOR_DESCONECTADO'
                                self.dosificador._bombaVariador.reconectarVariador()
                                time.sleep(5)
                    elif (RaspberryGPIO.obtenerValorCalibrado != "AUTOMATICO" and RaspberryGPIO.obtenerValorCalibrado() != "MANUAL"):
                        try:
                            self.dosificador._bombaVariador.pararVariador()
                        except Exception as identifier:
                            print("No se pudo escribir en bomba,Reconectando comunicacion",identifier)
                            self.dosificador._mensajeError = 'MSG_002_VARIADOR_DESCONECTADO'
                            self.dosificador._bombaVariador.reconectarVariador()
                            time.sleep(0.5)
                    
                    #------SOCKET CENTRAL-------#
          
                    if self.dosificador._socketCentral._connected == False:
                        self.dosificador._socketCentral.reconectarSocket()
                    info = { 'dosificadorId': self.dosificador._dosificador_id,
                                'alias':self.dosificador._dosificador_alias,
                                'litrosPorTonelada': self.dosificador._estadisticas.litrosPortonelada(),
                                'mililitrosManual':self.dosificador._estadisticas.GetmililitrosMinutoManual(),
                                'toneladasPorHora': self.dosificador._estadisticas.GetToneladasHora(),
                                'toneladasAcumuladas': self.dosificador._estadisticas.GetToneladasAcumuladas(),
                                'estado': RaspberryGPIO.obtenerValorCalibrado(),
                                'imagenesPorSegundo': SensorValle.getTiempoCiclo(),
                                'litrosAplicados': self.dosificador._estadisticas.GetLittrosAplicados() ,
                                'errores':self.dosificador._mensajeError ,
                                'kgvalle':self.dosificador._estadisticas.GetKgValle(),
                                'llenadoReal':self.dosificador._bombaVariador.getPorcentajeLlenadoReal(),
                                'setLLenado':self.dosificador._enviarSetLllenado ,
                                'velocidadBomba':self.dosificador._bombaVariador.GetVelocidadVariador()}
                    self.dosificador._socketCentral.emitReportStatus(info)
                    self.dosificador._bombaVariador.GetVelocidadVariador()
                    self.dosificador.tiempoPulsoFinal = time.time() 
                    self.dosificador.actualizarVelocidadMesas()
            
                else: 
                    if ((time.time() - self.dosificador.tiempoPulsoFinal) > 5 ): 
                        self.dosificador._bombaVariador.pararVariador()
                        self.dosificador.tiempoPulsoFinal = time.time()

            except CintaParadaException:
                self.dosificador._mensajeError = 'MSG_001_CINTA_PARADA'
                self.dosificador._intercomunidador.ponerEnceroDb()
                #Socket
                self.dosificador.objIntercomunicacion = self.dosificador._intercomunidador.leerDatosDesdeDB()
                self.dosificador._litrosAplicados = "{0:.3f}".format(self.dosificador.objIntercomunicacion["LITROS_APLICADOS"])
                self.dosificador._toneladasAcumuladas = "{0:.3f}".format(self.dosificador.objIntercomunicacion["TONELADAS_ACUMULADAS"])
                self.dosificador.enviarDatosCentral()
                self.dosificador.actualizarDatos()
                time.sleep(0.1)
                print(">>> LA CINTA ESTA PARADA <<<")          
                

            except:
                print('ERROR, proceso method has failed')
                traceback.print_exc()

        