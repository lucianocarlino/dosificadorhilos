import socketio
from Configuracion import Configuracion
from Intercomunicador import Intercomunicador
import time
from lib.actuacion.Estadisticas import Estadisticas

class Socket():
  def __init__(self, objEstadisticas):
    
    Configuracion()
    self._inter=Intercomunicador()
    self._servidorCentral = Configuracion.getValor("servidor_central")
    self._servidorCentralPuerto = Configuracion.getValor("servidor_central_puerto")
    self._connected = False
    self._tiempoReconexion = time.time()
    self._sio = socketio.Client()
    self._estadisticas = objEstadisticas
    @self._sio.event
    def connect():
      self._connected = True
      print('::: Socket Conectado :::')
    @self._sio.event
    def disconnect():
      self._connected = False
      print('::: Socket Desconectado :::')
    @self._sio.on('eventos.dosificadores.changeStatusBroadcast')
    def socketChangeConfig(objConfiguracion):
     self.eventChangeConfig(objConfiguracion)
    
    try:
      self._sio.connect('http://'+self._servidorCentral+':'+self._servidorCentralPuerto)
    except Exception as identifier:
      print("===> NO Hay Conexion al Socket <===")
      self._connected = False
    print('[Socket] >>> [Socket Iniciado Correctamente]')

  def reconectarSocket(self):
    if(time.time() - self._tiempoReconexion >40 ):
      @self._sio.event
      def connect():
        self._connected = True
        print('::: Socket Conectado :::')
      @self._sio.event
      def disconnect():
        self._connected = False
        print('::: Socket Desconectado :::')

      @self._sio.on('eventos.dosificadores.changeStatusBroadcast')
      def socketChangeConfig(objConfiguracion):
        self.eventChangeConfig(objConfiguracion)
      try:
        self._sio.connect('http://'+self._servidorCentral+':'+self._servidorCentralPuerto)
      except Exception as identifier:
        self._tiempoReconexion = time.time()
        print("===> NO Hay Conneccion al Socket <===")
        self._connected = False
  
  def eventChangeConfig(self,objConfiguracion):
    dosificadores = objConfiguracion["dosificadorId"].split(",")
    for dosificadorId in dosificadores:
        if(dosificadorId == Configuracion.getValor("dosificador_id")):
          if 'litrosAplicados' in objConfiguracion:
            self._inter.ponerENceromanualDB()
            self._estadisticas.ponerValoresEnCero()
           
          if 'litrosPorTonelada' in objConfiguracion:
            Configuracion.setValor("estadistica.litros_por_tonelada",objConfiguracion["litrosPorTonelada"])
          if 'mililitrosPorMinuto' in objConfiguracion:
            Configuracion.setValor("estadisticas.mililitros_por_minuto_manual",objConfiguracion["mililitrosPorMinuto"])
          if 'porcentajeLLenado' in objConfiguracion:
            Configuracion.setValor("porcentaje_llenado",objConfiguracion["porcentajeLLenado"]) 
          if 'dosificadorAlias' in objConfiguracion:
            if 'dosificadorAlias' != None:
              Configuracion.setValor("dosificador_alias",objConfiguracion["dosificadorAlias"]) 
  
  def isSocketConnected(self):
    return self._connected
  
  def emitReportStatus(self,info):
    try:
      self._sio.emit('eventos.dosificadores.reportStatus', info)
    except Exception as identifier:
      self.reconectarSocket()
