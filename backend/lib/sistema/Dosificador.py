import time
from Configuracion import Configuracion
from RaspberryGPIO import RaspberryGPIO
from lib.adquisicion.Camara import Camara
from lib.adquisicion.SensorValle import SensorValle
from lib.actuacion.Estadisticas import Estadisticas
from lib.actuacion.Bomba import Bomba
from errores.CintaParadaException import CintaParadaException
from Intercomunicador import Intercomunicador
from lib.sistema.Socket import Socket
from lib.adquisicion.SocketBridge import SocketBridge
import threading
import json
from lib.adquisicion.SocketpulsoSend import SocketpulsoSend
from lib.actuacion.Funcionamiento import Funcionamiento
from datetime import date
from datetime import datetime
import os


class Dosificador():
  
  def __init__(self):
    # 1- cargar los datos de configuracion (crear objeto estatico)
    # 2- instanciar GPIO (crear objeto estatico)
    # 3- crear objetos camara
    # 4- crear objeto sensor valle
    print("--- Iniciando Dispositivos ---")
    Configuracion()
    RaspberryGPIO() 
    SensorValle()
    
    #self._socketcomunicacion = SocketBridge()
    #self._socketcomunicacion.iniciarServidor()
    #self._socketsendpulso=SocketpulsoSend()
    self._dosificador_id = Configuracion.getValor("dosificador_id")
    self._dosificador_alias =Configuracion.getValor("dosificador_alias")

    for i in range (int(Configuracion.getValor("cantidad_camaras"))):
        camara = Camara(i, f"camara_{i+1}")
        self.camaras.append(camara)

    if((int(Configuracion.getValor("cantidad_camaras"))) == 1):
      self._camaraUno = Camara(0,"camara_uno")
      
    
    if((int(Configuracion.getValor("cantidad_camaras"))) == 2):
      self._camaraUno = Camara(0,"camara_uno")
      self._camaraDos = Camara(2,"camara_dos")
      
    
    if((int(Configuracion.getValor("cantidad_camaras"))) == 3):
      self._camaraUno = Camara(0,"camara_uno")
      self._camaraDos = Camara(2,"camara_dos")
      self._camaraTres = Camara(4,"camara_tres")
    
    if((int(Configuracion.getValor("cantidad_camaras"))) == 4):
      self._camaraUno = Camara(0,"camara_uno")
      self._camaraDos = Camara(2,"camara_dos")
      self._camaraTres = Camara(4,"camara_tres")
      self._camaraCuatro = Camara(6,"camara_cuatro")
    
    self._bombaVariador = Bomba()
    self._intercomunidador = Intercomunicador()
    objIntercomunicacion = self._intercomunidador.leerDatosDesdeDB()
    self._setToneladasAcumuladasDb=objIntercomunicacion["TONELADAS_ACUMULADAS"]
    self._setLitrosAplicadosDb=objIntercomunicacion["LITROS_APLICADOS"]
    self._estadisticas = Estadisticas(self._setLitrosAplicadosDb,self._setToneladasAcumuladasDb)
    self._socketCentral = Socket(self._estadisticas)
    self.tiempoPulsoFinal=time.time() #agregar programa nuevo dosificadores
    self._porcentajeLLenado=0
    self._calcular = False
    self._setLLenado = int (Configuracion.getValor("porcentaje_llenado"))
    self._valorkgvalle=0.00
    self._valorporcentaje=0.00
    self._tiempoUltimoInsert = time.time()
    self._tiempoUltimoInsertCentral = time.time()
    
    self._registroFuncionamiento=Funcionamiento()
    self._mensajeError="--- EQUIPO ENCENDIDO ---"
    
    self._registroFuncionamiento.GuardarDatosTxt("---INICIO DE PROCESO---",self._mensajeError,self._setToneladasAcumuladasDb,self._setLitrosAplicadosDb,self._estadisticas.litrosPortonelada())
    self._litrosAplicados=0
    self._toneladasAcumuladas=0
    self._pulsosReset=0
  def iniciarDosificador(self):

    print("")
    print("--- Dosificador Funcionando Correctamente ---")
    
    while(True):
      
      try:
        self.actualizarVelocidadMesas()
        self.actualizarDatos()

        #self._registroFuncionamiento.GuardarDatosTxt(RaspberryGPIO.obtenerValorCalibrado(),self._mensajeError,self._estadisticas.GetToneladasAcumuladas(),self._estadisticas.GetLittrosAplicados(),self._estadisticas.litrosPortonelada())

        
        if(SensorValle.debeTomarFoto()==True):
          #self._registroFuncionamiento.GuardarDatosTxt(RaspberryGPIO.obtenerValorCalibrado(),self._mensajeError,self._estadisticas.GetToneladasAcumuladas(),self._estadisticas.GetLittrosAplicados(),self._estadisticas.litrosPortonelada())
          print("<------------------------------------------------------>")
          self._bombaVariador.escribirVariadorMesaUno(self._porcentajeLLenado,self._calcular,self._setLLenado,SensorValle.getTiempocicloAnterior())
          self._calcular = False
          self._pulsosReset=self._pulsosReset+1
          #if self._pulsosReset >= 5:
          #  os.system('clear')
          #  self._pulsosReset=0


          #------------SOCKET COMUNICACION ESCLAVO ---------------#
          #if self._socketcomunicacion.EstadoDeSocket()==True:  
          #  self._valorkgvalle=self._socketcomunicacion.valorRecibidoKgvalle()
          #  self._valorporcentaje=self._socketcomunicacion.valorRecibidoPorcentajeLlenado()
          #else:
          #  self._socketcomunicacion.iniciarServidor()
          self._mensajeError = 'MSG_000_SIN_ERROR'
          try:
            #TODO Aqui se guardan los kilogramos por valle. Es el punto de quiebre, donde deberia poner la imagen en cola y luego sacar una imagen de la cola
            self._kgtotal = self._camaraUno.calcularKgFruta()#+self._camaraDos.calcularKgFruta()#+float (self._valorkgvalle)  
          except Exception as identifier:
            self._kgtotal=0
          
          self._porcentajeLLenado = (self._camaraUno.llenado() )#+ self._camaraDos.llenado()) #+ self._valorporcentaje) 
          self._calcular = True
          self._enviarSetLllenado="{0:.0f}".format((self._setLLenado)*3)
          print ("---- KG VALLE ---- ","{:.3f}".format(float(str(self._kgtotal))))
          print ("---- LLENADO ---- ","{:.3f}".format(float(str(self._porcentajeLLenado))))
          #print ("---- SET LLENADO ---- ","{:.3f}".format(float(str(self._setLLenado))))
          if self._kgtotal > 0.080 and RaspberryGPIO.obtenerValorCalibrado()=="AUTOMATICO":
            self._kgtotalSend = self._kgtotal
          else:
            self._kgtotalSend=0.001
          self._estadisticas.setKilogramosValleTotal(self._kgtotalSend)
          self._estadisticas.setTiempoCiclo(SensorValle.getTiempoCiclo)
          try:
            self.guardarDatosEstadisticos()  
          except Exception as identifier:
            print("no se pudo guardar en base de datos",identifier) 
          self._intercomunidador.guardarDatosEnDB()
          if (RaspberryGPIO.obtenerValorCalibrado() == "MANUAL"):
            print ("---- Manual ----")
            Configuracion.loadConfiguracionFromDatabase()
            # self._setLLenado= int(Configuracion.getValor("porcentaje_llenado"))
            mililitrosPorMinutos = float(Configuracion.getValor("estadisticas.mililitros_por_minuto_manual"))
            self._estadisticas.mililitrosPorminutoManual(mililitrosPorMinutos)
            self._bombaVariador.escribirEnVariador(mililitrosPorMinutos)
          if (RaspberryGPIO.obtenerValorCalibrado() != "MANUAL" and RaspberryGPIO.obtenerValorCalibrado()!= "AUTOMATICO"):
            try:
                self._bombaVariador.pararVariador()
            except Exception as identifier:
                print("No se pudo escribir en bomba,Reconectando comunicacion",identifier)
                self._bombaVariador.reconectarVariador()
                self._mensajeError = 'MSG_002_VARIADOR_DESCONECTADO'
                time.sleep(0.5)
          if (RaspberryGPIO.obtenerValorCalibrado()== "AUTOMATICO"):
            print ("---- Automatico ----")
            #print("----- TIEMPO CICLO ----",SensorValle.getTiempoCiclo())
            if self._kgtotal > 0.100:
              try:
                self._bombaVariador.escribirEnVariador(self._estadisticas.mililitrosPorminuto())
              except Exception as identifier:
                  print("No se pudo escribir en bomba,Reconectando comunicacion",identifier)
                  self._mensajeError = 'MSG_002_VARIADOR_DESCONECTADO'
                  self._bombaVariador.reconectarVariador()
                  time.sleep(0.5)
            if self._kgtotal < 0.100:
              self._intercomunidador.ponerEnceroDb()
              try:
                self._bombaVariador.pararVariador()
              except Exception as identifier:
                print("No se pudo escribir en bomba,Reconectando comunicacion",identifier)
                self._mensajeError = 'MSG_002_VARIADOR_DESCONECTADO'
                self._bombaVariador.reconectarVariador()
                time.sleep(5)
          #------SOCKET CENTRAL-------#
          
          if self._socketCentral._connected == False:
            self._socketCentral.reconectarSocket()
          info = { 'dosificadorId': self._dosificador_id,
                    'alias':self._dosificador_alias,
                    'litrosPorTonelada': self._estadisticas.litrosPortonelada(),
                    'mililitrosManual':self._estadisticas.GetmililitrosMinutoManual(),
                    'toneladasPorHora': self._estadisticas.GetToneladasHora(),
                    'toneladasAcumuladas': self._estadisticas.GetToneladasAcumuladas(),
                    'estado': RaspberryGPIO.obtenerValorCalibrado(),
                    'imagenesPorSegundo': SensorValle.getTiempoCiclo(),
                    'litrosAplicados': self._estadisticas.GetLittrosAplicados() ,
                    'errores':self._mensajeError ,
                    'kgvalle':self._estadisticas.GetKgValle(),
                    'llenadoReal':self._bombaVariador.getPorcentajeLlenadoReal(),
                    'setLLenado':self._enviarSetLllenado ,
                    'velocidadBomba':self._bombaVariador.GetVelocidadVariador()}
          self._socketCentral.emitReportStatus(info)
          self._bombaVariador.GetVelocidadVariador()
          self.tiempoPulsoFinal = time.time() # agregar programa nuevo dosificadores
          self.actualizarVelocidadMesas()
          print("<------------------------------------------------------>")


        else: #agregar programa nuevo dosificadres

          if ((time.time() - self.tiempoPulsoFinal) > 5 ): #agregar programa nuevo dosificadres
            self._bombaVariador.pararVariador()#agregar programa nuevo dosificadres
            self.tiempoPulsoFinal = time.time()#agregar programa nuevo dosificadres
      
      except CintaParadaException:
        self._mensajeError = 'MSG_001_CINTA_PARADA'
        self._intercomunidador.ponerEnceroDb()
        #Socket
        objIntercomunicacion = self._intercomunidador.leerDatosDesdeDB()
        self._litrosAplicados = "{0:.3f}".format(objIntercomunicacion["LITROS_APLICADOS"])
        self._toneladasAcumuladas = "{0:.3f}".format(objIntercomunicacion["TONELADAS_ACUMULADAS"])
        self.enviarDatosCentral()
        self.actualizarDatos()
        time.sleep(0.1)
        print(">>> LA CINTA ESTA PARADA <<<")

  def actualizarDatos(self):
    if(time.time() - self._tiempoUltimoInsert >5 ):
      Configuracion.loadConfiguracionFromDatabase()
      self._setLLenadoBaseDeDatos= int(Configuracion.getValor("porcentaje_llenado"))
      self._setLLenado= self._setLLenadoBaseDeDatos /  3
      self._dosificador_alias= Configuracion.getValor("dosificador_alias")
      mililitrosPorMinutos = float(Configuracion.getValor("estadisticas.mililitros_por_minuto_manual"))
      self._estadisticas.mililitrosPorminutoManual(mililitrosPorMinutos)
      self._tiempoUltimoInsert = time.time()

  def enviarDatosCentral(self):
    if self._socketCentral._connected == False:
      self._socketCentral.reconectarSocket()
    self._setLLenadoBaseDeDatos= int(Configuracion.getValor("porcentaje_llenado"))
    if(time.time() - self._tiempoUltimoInsertCentral >2 ):
      info = { 'dosificadorId': self._dosificador_id,
               'alias':self._dosificador_alias,
               'litrosPorTonelada':self._estadisticas.litrosPortonelada(),
               'toneladasPorHora': 0,
               'toneladasAcumuladas': self._toneladasAcumuladas , 
               'estado': RaspberryGPIO.obtenerValorCalibrado(),
               'imagenesPorSegundo': 0, 
               'litrosAplicados': self._litrosAplicados ,
               'mensaje': self._mensajeError,
               'kgvalle':0 ,
               'llenadoReal':0 ,
               'setLLenado':self._setLLenadoBaseDeDatos ,
               'velocidadBomba':self._bombaVariador.GetVelocidadVariador(),
               'errores':self._mensajeError,
               'mililitrosManual':self._estadisticas.GetmililitrosMinutoManual(),
               }
      self._socketCentral.emitReportStatus(info)
      self._tiempoUltimoInsertCentral=time.time()
    
  def recargarConfiguracionCamaras(self):
    if((int(Configuracion.getValor("cantidad_camaras"))) == 1):
      self._camaraUno.loadConfiguracionCamara("camara_uno")

    if((int(Configuracion.getValor("cantidad_camaras"))) == 2):
      self._camaraUno.loadConfiguracionCamara("camara_uno")
      self._camaraDos.loadConfiguracionCamara("camara_dos")
    
    if((int(Configuracion.getValor("cantidad_camaras"))) == 3):
      self._camaraUno.loadConfiguracionCamara("camara_uno")
      self._camaraDos.loadConfiguracionCamara("camara_dos")
      self._camaraTres.loadConfiguracionCamara("camara_tres")
    
    if((int(Configuracion.getValor("cantidad_camaras"))) == 4):
      self._camaraUno.loadConfiguracionCamara("camara_uno")
      self._camaraDos.loadConfiguracionCamara("camara_dos")
      self._camaraTres.loadConfiguracionCamara("camara_tres")
      self._camaraCuatro.loadConfiguracionCamara("camara_cuatro")

  def guardarDatosEstadisticos(self):
    self._intercomunidador.setKilogramosValleTotal(self._kgtotal)#self._camaraUno.calcularKgFruta() + self._camaraDos = Camara(1,"camara_dos")
    self._intercomunidador.setkilogramosPorHora( self._estadisticas.kilogramosPorhora())
    self._intercomunidador.settiempoCiclo(SensorValle._tiempoCiclo)
    self._intercomunidador.setmililitrosPorMinuto(self._estadisticas.mililitrosPorminuto())
    self._intercomunidador.settiempoLlenadoDeJeringa(self._estadisticas.tiempoLLenadodeJeringapara60ml())
    self._intercomunidador.settoneladasAcumuladas(self._estadisticas.toneladasAcumuladas())
    self._intercomunidador.setlitrosAplicados(self._estadisticas.litrosAplicados())
    self._intercomunidador.setlitrosPorTonelada(self._estadisticas.litrosPortonelada())
    self._intercomunidador.settoneladasPorHora(self._estadisticas.toneladasHora())
    self._intercomunidador.setvelocidadVariador(self._bombaVariador.leerVelocidadenHertz())
    self._intercomunidador.setestado(RaspberryGPIO.obtenerValorCalibrado())

  def actualizarVelocidadMesas(self):
      # self._bombaVariador.escribirVariadorMesaDos(self._porcentajeLLenado,self._calcular,self._setLLenado)
    if time.time() > self._bombaVariador.tiempoiniciofrena() and time.time() < self._bombaVariador.tiempofrena():
      RaspberryGPIO.frenaVariadorMesaInspeccion()
     # print("frena")
    if time.time() > self._bombaVariador.tiempofrena():
      #print ("termina frenar")
      RaspberryGPIO.terminaFrenarVariadorMesaInspeccion()
    if time.time() > self._bombaVariador.tiempoinicioacelera() and time.time() < self._bombaVariador.tiempoacelera():
     # print("acelera",self._bombaVariador.tiempoinicioacelera(),time.time() ,self._bombaVariador.tiempoacelera())
      RaspberryGPIO.aceleraVariadorMesaInspeccion()
    if time.time() > self._bombaVariador.tiempoacelera():
     # print("termina acelerar")
      RaspberryGPIO.terminaAcelerarVariadorMesaInspeccion()
