import time
from random import seed
from random import randint
from RaspberryGPIO import RaspberryGPIO
from errores.CintaParadaException import CintaParadaException
import statistics

class SensorValle():

  def __init__(self):
    SensorValle._tiempoCiclo = 0
    SensorValle._anterior = False
    SensorValle._actual = False
    SensorValle._iniancho = 0
    SensorValle._financho = 0
    SensorValle._inipulso = 0
    SensorValle._finpulso = 0
    SensorValle._pulsos = 1
    SensorValle._cuentapulsos = 0
    SensorValle._finpulsoant = 0
    SensorValle._tamañoRegistroVelocidadMesa=10
    SensorValle._estadisticaEscrituraMesaInspeccion=list(range(SensorValle._tamañoRegistroVelocidadMesa,0))
    SensorValle._resetMesaInspeccion=True
    for m in range (SensorValle._tamañoRegistroVelocidadMesa):
        SensorValle._estadisticaEscrituraMesaInspeccion.append(0)
    SensorValle._valorEstadisticoMesa=0
    SensorValle._rangotiempoanterior=2
    SensorValle._tiempoanterior=list(range(SensorValle._rangotiempoanterior,0))
    for m in range (SensorValle._rangotiempoanterior):
        SensorValle._tiempoanterior.append(0)
    SensorValle._tiempoTrue=time.time()
    SensorValle._tiempoFalse=time.time()
    print('[SensorValle] >>> [Iniciado Correctamente]')
       

  @staticmethod 
  def debeTomarFoto():
     SensorValle._actual =  RaspberryGPIO.obtenerValorSendorOptico()
     
     if  (SensorValle._actual == True and SensorValle._anterior == False):
           SensorValle._iniancho = time.time()
     if  (SensorValle._actual == False and SensorValle._anterior == True):
           SensorValle._financho = time.time()
     if  ((SensorValle._financho - SensorValle._iniancho) > 0.080):
           SensorValle._inipulso = time.time() - SensorValle._iniancho + SensorValle._financho
           SensorValle._iniancho = SensorValle._financho
           SensorValle._cuentapulsos = SensorValle._cuentapulsos + 1
           if (SensorValle._cuentapulsos == SensorValle._pulsos):
                 SensorValle._finpulso = time.time()
                 SensorValle._tiempoCiclo = SensorValle._finpulso - SensorValle._finpulsoant 
                 SensorValle._cuentapulsos = 0
                 SensorValle._anterior = SensorValle._actual
                 SensorValle._finpulsoant = SensorValle._finpulso
                 RaspberryGPIO.encenderLedFuncionamiento()
                 return True
           else: 
              return False    
     else:
            SensorValle._anterior = SensorValle._actual
            if (time.time() - SensorValle._finpulsoant > 0.2):
              RaspberryGPIO.apagarLedFuncionamiento()
  
            if(SensorValle.checkearCintaParada() == True):
              raise CintaParadaException() 

            return False

  @staticmethod 
  def debeTomarFoto2():
    if(time.time()-SensorValle._tiempoTrue > 0.5):
      SensorValle._tiempoCiclo=time.time()-SensorValle._tiempoTrue
      SensorValle._tiempoTrue = time.time()
      return True
    else:
      return False
  @staticmethod
  def getTiempoCiclo():
   
    del SensorValle._estadisticaEscrituraMesaInspeccion[0]
    SensorValle._estadisticaEscrituraMesaInspeccion.append(SensorValle._tiempoCiclo)
    SensorValle._valorEstadisticoMesa= sum(SensorValle._estadisticaEscrituraMesaInspeccion)
    porcentajeTotal =SensorValle._valorEstadisticoMesa/SensorValle._tamañoRegistroVelocidadMesa
    #print( 'TIEMPO CICLO',"{:.3f}".format(float(SensorValle._tiempoCiclo)))
    #print ("------DESVIACION ESTANDAR------- ","{:.3f}".format(float(statistics.stdev(SensorValle._estadisticaEscrituraMesaInspeccion))))
    return "{:.2f}".format(float(SensorValle._tiempoCiclo))
  @staticmethod
  def getTiempocicloAnterior():
   
    del SensorValle._tiempoanterior[0]
    SensorValle._tiempoanterior.append(SensorValle._tiempoCiclo)
    tiempoanterior= SensorValle._tiempoanterior[0]
    #print("tiempo anterior,""{:.3f}".format(float(tiempoanterior)))
    return "{:.3f}".format(float(tiempoanterior))

  @staticmethod 
  def checkearCintaParada():
    
    if((time.time() - SensorValle._finpulsoant) > 10):
      return True
    else:
      return False
  @staticmethod 
  def debeTomarFotoCalibracion2():
    if(time.time()-SensorValle._tiempoTrue > 0.5):
      SensorValle._tiempoCiclo=time.time()-SensorValle._tiempoTrue
      SensorValle._tiempoTrue = time.time()
      return True
    else:
      return False
  @staticmethod 
  def debeTomarFotoCalibracion():
     SensorValle._actual =  RaspberryGPIO.obtenerValorSendorOptico()
     
     if  (SensorValle._actual == True and SensorValle._anterior == False):
           SensorValle._iniancho = time.time()
     if  (SensorValle._actual == False and SensorValle._anterior == True):
           SensorValle._financho = time.time()
     if  ((SensorValle._financho - SensorValle._iniancho) > 0.015):
           SensorValle._inipulso = time.time() - SensorValle._iniancho + SensorValle._financho
           SensorValle._iniancho = SensorValle._financho
           SensorValle._cuentapulsos = SensorValle._cuentapulsos + 1
           if (SensorValle._cuentapulsos == SensorValle._pulsos):
                 SensorValle._finpulso = time.time()
                 SensorValle._tiempoCiclo = SensorValle._finpulso - SensorValle._finpulsoant 
                 SensorValle._cuentapulsos = 0
                 SensorValle._anterior = SensorValle._actual
                 SensorValle._finpulsoant = SensorValle._finpulso
                 RaspberryGPIO.encenderLedFuncionamiento()
                 return True
           else: 
              return False    
     else:
            SensorValle._anterior = SensorValle._actual
            if (time.time() - SensorValle._finpulsoant > 0.1):
              RaspberryGPIO.apagarLedFuncionamiento()
   