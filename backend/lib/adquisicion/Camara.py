import cv2
import numpy as np
from Configuracion import Configuracion
class Camara():
                      #,nombreCamara                                       #"+camara_uno+"
  def __init__(self,numeroCamara,nombreCamara):
    self._nombreCamara = nombreCamara
    self._camara = cv2.VideoCapture(numeroCamara)
    self._camara.set(cv2.CAP_PROP_FRAME_WIDTH, 900)
    self._camara.set(cv2.CAP_PROP_FRAME_HEIGHT, 600)
    self._camara.set(cv2.CAP_PROP_FPS, 60)
    self._colorRegioninteresCamara = 0,0,255
    self._espesorLinearegionInterescamara = 1
    self.loadConfiguracionCamara(nombreCamara)
    try:
      self._camara.read()
    except Exception as identifier:
      print("no pudo iniciar camara" , identifier)
    # self._puntoUnoX = int(Configuracion.getValor("camara."+self._nombreCamara+".punto_uno_x"))
    # self._puntoUnoY = int(Configuracion.getValor("camara."+self._nombreCamara+".punto_uno_y"))
    # self._puntoDosX = int(Configuracion.getValor("camara."+self._nombreCamara+".punto_dos_x"))
    # self._puntoDosY = int(Configuracion.getValor("camara."+self._nombreCamara+".punto_dos_y"))
    # print(self._nombreCamara + " --> PuntoUnoX: {}  PuntoUnoY: {}  PuntoDosX: {}  PuntoDosY: {}".format(self._puntoUnoX,self._puntoUnoY,self._puntoDosX,self._puntoDosY))
    #self._camara = cv2.VideoCapture(numeroCamara)
    # self._anchoRegioninteresCamara = self._puntoUnoX ,self._puntoUnoY
    # self._altoRegioninteresCamara = self._puntoDosX , self._puntoDosY
    # self._verdeBajoscolor = int(Configuracion.getValor("camara."+self._nombreCamara+".verdes_bajos_color"))
    # self._variableCalculopesoPorkg = float(Configuracion.getValor("camara."+self._nombreCamara+".pixel_por_kilogramo"))
    # self._camara.read()

    print('[Camara] >>> [Iniciada Correctamente]')

  def loadConfiguracionCamara(self,nombreCamara):
    self._puntoUnoX = int(Configuracion.getValor("camara."+self._nombreCamara+".punto_uno_x"))
    self._puntoUnoY = int(Configuracion.getValor("camara."+self._nombreCamara+".punto_uno_y"))
    self._puntoDosX = int(Configuracion.getValor("camara."+self._nombreCamara+".punto_dos_x"))
    self._puntoDosY = int(Configuracion.getValor("camara."+self._nombreCamara+".punto_dos_y"))
    self._anchoRegioninteresCamara = self._puntoUnoX ,self._puntoUnoY
    self._altoRegioninteresCamara = self._puntoDosX , self._puntoDosY
    self._verdeBajoscolor = int(Configuracion.getValor("camara."+self._nombreCamara+".verdes_bajos_color"))
    self._variableCalculopesoPorkg = float(Configuracion.getValor("camara."+self._nombreCamara+".pixel_por_kilogramo"))
    

  def sacarFoto(self):
    try:
       self._imagenFueTomada, self._imagenTomada = self._camara.read()
       #self._imagenTomada =cv2.imread('/home/pi/limon2.jpg',1)
       self._rectanguloAreainteres = cv2.rectangle(self._imagenTomada , self._anchoRegioninteresCamara , self._altoRegioninteresCamara , self._colorRegioninteresCamara , self._espesorLinearegionInterescamara)  
       self._cropped = self._imagenTomada[self._puntoUnoY:self._puntoDosY, self._puntoUnoX:self._puntoDosX]
       #cv2.imshow(self._nombreCamara,  self._imagenTomada)
       #if cv2.waitKey(1) & 0xFF == ord('q'):
       #      cv2.destroyAllWindows()  
    except Exception as identifier:
       print("No se pudo sacar foto")
       self._camara.read()

  def sacarFoto(self):
    self._imagenFueTomada, self._imagenTomada = self._camara.read()
    #self._imagenTomada =cv2.imread('/home/pi/limon2.jpg',1)
    self._rectanguloAreainteres = cv2.rectangle(self._imagenTomada , self._anchoRegioninteresCamara , self._altoRegioninteresCamara , self._colorRegioninteresCamara , self._espesorLinearegionInterescamara)  
    self._cropped = self._imagenTomada[self._puntoUnoY:self._puntoDosY, self._puntoUnoX:self._puntoDosX]
    #cv2.imshow(self._nombreCamara,  self._imagenTomada)
    #if cv2.waitKey(1) & 0xFF == ord('q'):
    #cv2.destroyAllWindows()
    return self.cropped

   

  def convertirMatrizNueva(self):
    try:
      self._imagenPixelesEnBlue,self._imagenPixelesEnGreen,self._imagenPixelesEnRed = cv2.split(self._cropped)
      ajusteFuncionMatricialUno = int(Configuracion.getValor("camara."+self._nombreCamara+".ajuste_uno_funcion_matricial"))
      ajusteFuncionMatricialDos = float(Configuracion.getValor("camara."+self._nombreCamara+".ajuste_dos_funcion_matricial"))
      self._imagenTransformadaParaCalculo = (self._imagenPixelesEnGreen-ajusteFuncionMatricialUno)*ajusteFuncionMatricialDos-(self._imagenPixelesEnBlue)
      self._nuevaMatriz = cv2.merge((self._imagenTransformadaParaCalculo,self._imagenTransformadaParaCalculo,self._imagenTransformadaParaCalculo))
      self._verde_bajos = np.array([0,0,0], dtype=np.uint8)
      self._verde_altos = np.array([self._verdeBajoscolor,255,255], dtype=np.uint8)
      self._mascara = cv2.inRange(self._nuevaMatriz, self._verde_bajos,self._verde_altos ) 
    
     # cv2.imshow(self._nombreCamara+"camara2",self._mascara)
     # if cv2.waitKey(1) & 0xFF == ord('q'):
     #             cv2.destroyAllWindows() 
    except Exception as identifier:
      print("No se pudo generar matriz nueva")
     
    
    
 
  def calcularPixelesPorKilogramos(self):
    try:
      #self._centroFruta = cv2.moments(self._mascara)
      #self._pixeles = self._centroFruta['m00']
      #self._pixelesDividido= self._pixeles / 100
      self._pixelesBlancos=cv2.countNonZero(self._mascara)
     # print('----PIXELES BLANCOS',self._pixelesBlancos)
     # print('------PIXELES TOTALES-----',self._mascara.size)
      self._kilogramoPorvalle= (self._pixelesBlancos * self._variableCalculopesoPorkg )
      return self._kilogramoPorvalle
    except Exception as identifier:
      print("No se pudo calcular pixeles por kg")
      return 0
    
  
  def setPixelesFruta(self):
    try:
       self.calcularPixelesPorKilogramos()
       self._pixelesBlancos
       return self._pixelesBlancos
    except Exception as identifier:
       print("No se pudo calcular pixeles fruta")
       return 0
    

  def calcularKgFruta (self):
    try:
      self.sacarFoto()
      self.convertirMatrizNueva()
      self.calcularPixelesPorKilogramos()
      self._kilogramoPorvalle = ((self._pixelesBlancos) * self._variableCalculopesoPorkg )   
      return self._kilogramoPorvalle
    except Exception as identifier:
      print("No se pudo calcular peso de fruta")
      return 0
    
  
  def getImagenFromCamarareal(self):
    try:
      self.sacarFoto()
      self.convertirMatrizNueva()

      return self._imagenTomada
    except Exception as identifier:
      print("No se pudo mostrar imagen")
      return 0
    

  def getImagenFromCamaramascara(self):
    try:
      self._imagenFueTomada, self._imagenTomada = self._camara.read()
      #self._imagenTomada =cv2.imread('/home/pi/limon1.jpg',1)
      self._rectanguloAreainteres = cv2.rectangle(self._imagenTomada , self._anchoRegioninteresCamara , self._altoRegioninteresCamara , self._colorRegioninteresCamara , self._espesorLinearegionInterescamara)  
      self._cropped = self._imagenTomada[self._puntoUnoY:self._puntoDosY+60, self._puntoUnoX:self._puntoDosX+60]
      self._imagenPixelesEnBlue,self._imagenPixelesEnGreen,self._imagenPixelesEnRed = cv2.split(self._cropped)
      ajusteFuncionMatricialUno = int(Configuracion.getValor("camara."+self._nombreCamara+".ajuste_uno_funcion_matricial"))
      ajusteFuncionMatricialDos = float(Configuracion.getValor("camara."+self._nombreCamara+".ajuste_dos_funcion_matricial"))
      self._imagenTransformadaParaCalculo = (self._imagenPixelesEnGreen-ajusteFuncionMatricialUno)*ajusteFuncionMatricialDos-(self._imagenPixelesEnBlue)
      self._nuevaMatriz = cv2.merge((self._imagenTransformadaParaCalculo,self._imagenTransformadaParaCalculo,self._imagenTransformadaParaCalculo))
      self._verde_bajos = np.array([0,0,0], dtype=np.uint8)
      self._verde_altos = np.array([self._verdeBajoscolor,255,255], dtype=np.uint8)
      self._mascara = cv2.inRange(self._nuevaMatriz, self._verde_bajos,self._verde_altos ) 
      return self._mascara 
    except Exception as identifier:
      print("No se pudo sacar mascara")
    

  def getInstanciaCamara(self):
    return self._camara  

  def getNombreCamara(self):
    return self._nombreCamara

  def llenado(self):
    try:
      self.sacarFoto()
      self.convertirMatrizNueva()
      self._llenado=(self._pixelesBlancos / self._mascara.size )*100
     # print('---------LLENADO % ---------',"{:.0f}".format(float(self._llenado)))
      return self._llenado
    except Exception as identifier:
      print("No hay datos de llenado")
      return 0
    
