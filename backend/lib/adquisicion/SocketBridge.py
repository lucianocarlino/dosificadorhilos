import socket
import pickle
import time
import socket
import pickle
import threading
import time

class SocketBridge():
  def __init__(self):
    self._data_variable={'kgvalle': 0.00, 'porcentajellenado':0.00}
    self._estadoSocketDesconectado=True
  
  def iniciarServidor(self):
    self._data_variable={'kgvalle':0.00, 'porcentajellenado':0.00}
    self._estadoSocketDesconectado=True
    def worker():
      try:
         print("------SERVIDOR EN ESCUCHA-------")
         self._HOST = ''
         self._PORT = 9005
         self._s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
         self._s.bind((self._HOST, self._PORT))
         self._s.listen(1)
         self._conn, self._addr =self._s.accept()
         self._data_variable=0.00
         self._estadoSocketDesconectado=True
      except :
         self._estadoSocketDesconectado=False
     
      while(self._estadoSocketDesconectado==True):
        try:
          self._data = self._conn.recv(4096)
          self._data_variable = pickle.loads(self._data)
        except:
          self._estadoSocketDesconectado=False
          time.sleep(20)
          print('------SOCKET SERVIDOR DESCONECTADO-------')
    self._t = threading.Thread(target=worker)
    self._t.start()
  
  def valorRecibidoKgvalle(self):
     # print ('----KG VALLE ESCLAVO-----',self._data_variable['kgvalle'])
      return self._data_variable['kgvalle']
  
  def valorRecibidoPorcentajeLlenado(self):
    print ('----PORCENTAJE LLENADO ESCLAVO-----',self._data_variable['porcentajellenado'])
    return self._data_variable['porcentajellenado']
 
  def EstadoDeSocket(self):
    
    return self._estadoSocketDesconectado
