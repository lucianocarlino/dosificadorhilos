import traceback
import psycopg2
import time

class Configuracion():

  def __init__(self):
    Configuracion._dbName = 'dosificadordb'
    Configuracion._user = 'root'
    Configuracion._password = 'raspberry'
    Configuracion._host = 'localhost'
    Configuracion._port = 5432

    Configuracion._configuraciones = dict()
    Configuracion._ultimoRoundTripBaseDatos = time.time()
    Configuracion._ultimoRoundTripBaseDatosSet = time.time()
    Configuracion.loadConfiguracionFromDatabase(True)
    # logica de obtencion de la base de datos
    print('[Configuracion] >>> [Iniciada Correctamente]')

  @staticmethod
  def crearConexion():
    try:
      Configuracion._conexion=psycopg2.connect(dbname=Configuracion._dbName,
                                           user=Configuracion._user,
                                           password=Configuracion._password,
                                           host=Configuracion._host,
                                           port=Configuracion._port)
      if (Configuracion._conexion.closed):
        print('No se pudo conectar a la base de datos de configuracion')
      Configuracion.cursor = Configuracion._conexion.cursor()
    except:
            print('ERROR, conexion method has failed')
            traceback.print_exc()


  @staticmethod 
  def loadConfiguracionFromDatabase(ignoreRoundTripTime = False):
    Configuracion.crearConexion()
    if(ignoreRoundTripTime == True or time.time() - Configuracion._ultimoRoundTripBaseDatos > 0.2):
      cursor = Configuracion.cursor
      query = ("SELECT clave,valor FROM configuracion")
      cursor.execute(query)
      for (clave, valor) in cursor:
        Configuracion._configuraciones[clave] = valor 
      cursor.close()
      Configuracion._ultimoRoundTripBaseDatos = time.time()

  @staticmethod 
  def getValor(clave):
    return Configuracion._configuraciones.get(str(clave).strip())

  @staticmethod
  def setValor(clave,valor):
      Configuracion.crearConexion()
      cursor = Configuracion.cusor
      query = "UPDATE configuracion SET "\
              "valor = %s "\
              "WHERE clave = %s;"    
      values = (valor, clave)
      res = cursor.execute(query,values)
      cursor.execute(query, values)
      Configuracion._conexion.commi()
      cursor.close()
      Configuracion._conexion.close()

  @staticmethod
  def update(id, valor):
      Configuracion.crearConexion()
      cursor = Configuracion.cursor
      query = "UPDATE configuracion SET " \
              "valor = %s " \
              "WHERE id = %s;"
      values = (valor, id)
      cursor.execute(query, values)
      Configuracion._conexion.commit()
      cursor.close()
      Configuracion._conexion.close()