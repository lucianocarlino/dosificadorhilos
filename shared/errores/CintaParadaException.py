class CintaParadaException(Exception):
    def __init__(self):
        self.value = "### CINTA PARADA ###"
    def __str__(self):
        return repr(self.value)

class CamaraUnoDesconectada(Exception):
    def __init__(self):
        self.value = "### CAMARA UNO DESCONECTADA ###"
    def __str__(self):
        return repr(self.value)     

class CamaraDosDesconectada(Exception):
    def __init__(self):
        self.value = "### CAMARA DOS DESCONECTADA ###"
    def __str__(self):
        return repr(self.value)     

class VariadorDesconectado(Exception):
    def __init__(self):
        self.value = "### VARIADOR DESCONECTADO ###"
    def __str__(self):
        return repr(self.value)                      