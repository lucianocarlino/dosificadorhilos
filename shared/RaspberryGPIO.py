import RPi.GPIO as GPIO
import time
class RaspberryGPIO():

  def __init__(self):
    # obligatorio raspberry pi
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)

    # declaracion de entradas
    GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(25, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    #GPIO.setup(25, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    
    # declaracion salidas
    GPIO.setup(18, GPIO.OUT)
    GPIO.setup(26, GPIO.OUT)
    GPIO.setup(6, GPIO.OUT)
    
    print('[Raspberry GPIO] >>> [Iniciado Correctamente]')
  
  @staticmethod 
  def parsearBoolean(valor):
    if(valor == 1): return False
    if(valor == 0): return True
  
  @staticmethod 
  def obtenerValorSendorOptico():
    return RaspberryGPIO.parsearBoolean(GPIO.input(24))

  @staticmethod 
  def obtenerValorCalibrado():
    if(RaspberryGPIO.parsearBoolean(GPIO.input(25))): return "AUTOMATICO"
    if(RaspberryGPIO.parsearBoolean(GPIO.input(23))): return "MANUAL"
    if(not RaspberryGPIO.parsearBoolean(GPIO.input(23)) and not RaspberryGPIO.parsearBoolean(GPIO.input(25))):return "PAUSADO"
  @staticmethod 
  def checkearLlaveApagado():
    return RaspberryGPIO.parsearBoolean(GPIO.input(25))   

  @staticmethod 
  def encenderLedFuncionamiento():
    GPIO.output(18,True)
  
  @staticmethod 
  def apagarLedFuncionamiento():
    GPIO.output(18,False)   
     
  
  @staticmethod 
  def aceleraVariadorMesaInspeccion():
    GPIO.output(26,False) 
  
  @staticmethod 
  def terminaAcelerarVariadorMesaInspeccion():
    GPIO.output(26,True) 

  @staticmethod 
  def frenaVariadorMesaInspeccion():
    GPIO.output(6,False) 
  
  @staticmethod 
  def terminaFrenarVariadorMesaInspeccion():
    GPIO.output(6,True) 
  