import time
import psycopg2
import time

from Configuracion import Configuracion
from RaspberryGPIO import RaspberryGPIO
from lib.adquisicion.Camara import Camara
from lib.adquisicion.SensorValle import SensorValle
from lib.actuacion.Estadisticas import Estadisticas
from lib.actuacion.Bomba import Bomba
from errores.CintaParadaException import CintaParadaException

class Intercomunicador():
  
  def __init__(self):
    self._conexion = psycopg2.connect(dbname=Configuracion._dbName,
                                           user=Configuracion._user,
                                           password=Configuracion._password,
                                           host=Configuracion._host,
                                           port=Configuracion._port)
    objIntercomunicacion = self.leerDatosDesdeDB()
    self._kilogramosValleTotal = 0
    self._kilogramosPorHora = 0
    self._tiempoCiclo = 0
    self._mililitrosPorMinuto = 0
    self._tiempoLlenadoDeJeringa = 0 #Jeringa de 60 Mililitros
    self._toneladasAcumuladas = objIntercomunicacion["TONELADAS_ACUMULADAS"]
    self._litrosPorTonelada = 0
    self._toneladasPorHora = 0
    self._litrosAplicados = objIntercomunicacion["LITROS_APLICADOS"]
    self._velocidadVariador = 0 
    
    self._tiempoUltimoInsert = time.time()
    self.ponerEnceroDb() 
    print('[Intercomunicador] >>> [Iniciado Correctamente]')
     
  def setKilogramosValleTotal(self, kvt):
      self._kilogramosValleTotal = kvt
  
  def setkilogramosPorHora(self, kilogramosporhora):
    self._kilogramosPorHora = kilogramosporhora
    
  def settiempoCiclo(self, tiempociclo):
    self._tiempoCiclo = tiempociclo

  def setmililitrosPorMinuto(self, mililitrosporminuto):
    self._mililitrosPorMinuto = mililitrosporminuto
 
  def settiempoLlenadoDeJeringa(self, tiempollenadodejeringa):
    self._tiempoLlenadoDeJeringa = tiempollenadodejeringa

  def setlitrosAplicados(self, litrosaplicados):
    self._litrosAplicados = litrosaplicados

  def setlitrosPorTonelada(self, litrosportonelada):
    self._litrosPorTonelada = litrosportonelada

  def settoneladasPorHora(self, toneladasporhora):
    self._toneladasPorHora = toneladasporhora

  def setvelocidadVariador(self, velocidadvariador):
    self._velocidadVariador = velocidadvariador
  
  def settoneladasAcumuladas(self, toneladasacumuladas):
     self._toneladasAcumuladas = toneladasacumuladas
  
  def setestado(self, estado):
    self._estado=estado

  def guardarDatosEnDB(self):
    if(time.time() - self._tiempoUltimoInsert >20 ):
        print ("---------guardo datos-----")
        cursor = self._conexion.cursor()  
        query = "UPDATE intercomunicador SET "\
                "KILOGRAMOS_VALLE_TOTAL = %s,"\
                "KILOGRAMOS_POR_HORA = %s,"\
                "TIEMPO_CICLO = %s,"\
                "MILILITROS_POR_MINUTO = %s,"\
                "TIEMPO_LLENADO_JERINGA = %s,"\
                "TONELADAS_ACUMULADAS  = %s,"\
                "LITROS_APLICADOS = %s,"\
                "LITROS_POR_TONELADA = %s,"\
                "TONELADAS_POR_HORA = %s,"\
                "VELOCIDAD_VARIADOR  = %s,"\
                "ESTADO = %s"\
                "WHERE id = 4;"    
        values = (self._kilogramosValleTotal,self._kilogramosPorHora,self._tiempoCiclo,self._mililitrosPorMinuto,self._tiempoLlenadoDeJeringa,self._toneladasAcumuladas,self._litrosAplicados,self._litrosPorTonelada,self._toneladasPorHora,self._velocidadVariador,self._estado)   
        try:
          cursor.execute(query,values)
          self._conexion.commit ()
          cursor.close()
          self._tiempoUltimoInsert = time.time()
        except Exception as identifier:
          print(identifier)
        

        
  def leerDatosDesdeDB(self):
        cursor = self._conexion.cursor(buffered=False,dictionary = True)
        query = ("SELECT * FROM intercomunicador")
        try:
          self._conexion.cursor.execute(query)
          objConfiguracion = cursor.fetchone()
          cursor.close()
          return objConfiguracion
        except Exception as identifier:
          print(identifier)  
 
  def ponerEnceroDb(self):
        cursor = self._conexion.cursor()  
        query = "UPDATE intercomunicador SET "\
                "KILOGRAMOS_VALLE_TOTAL = %s,"\
                "KILOGRAMOS_POR_HORA = %s,"\
                "MILILITROS_POR_MINUTO = %s,"\
                "TIEMPO_LLENADO_JERINGA = %s,"\
                "TONELADAS_POR_HORA = %s,"\
                "VELOCIDAD_VARIADOR  = %s "\
                "WHERE id = 4;"    
        values = (0,0,0,0,0,0)   
        cursor.execute(query,values)
        self._conexion.commit ()
        cursor.close()    

  def ponerENceromanualDB(self):
        cursor = self._conexion.cursor()  
        query = "UPDATE intercomunicador SET "\
                "TONELADAS_ACUMULADAS = %s, "\
                "LITROS_APLICADOS = %s "\
                "WHERE id = 4;"    
        values = (0,0)   
        cursor.execute(query,values)
        self._cnx.commit ()
        cursor.close()    
