from dosificador.frontend.user_interface.frmPrincipal import frmPrincipal
from dosificador.frontend.user_interface.helpers.Autenticacion import Autenticacion
from dosificador.shared.Configuracion import Configuracion

Autenticacion()
Configuracion()
frmPrincipal.vp_start_gui()