import threading
import time
from dosificador.shared.Intercomunicador import Intercomunicador

class Updater(threading.Thread):
    def __init__(self,gui):
        self.formularioPrincipal = gui
        self._intercomunicador = Intercomunicador()
        threading.Thread.__init__(self)
        self.start()
    
    def run(self):
        loopActivo = True
        while(loopActivo):
            time.sleep(1)
            objIntercomunicacion = self._intercomunicador.leerDatosDesdeDB()
            #print(objIntercomunicacion)
            self.formularioPrincipal.lblLitrosAplicados.configure(text="{0:.3f}".format(objIntercomunicacion["LITROS_APLICADOS"]))
            self.formularioPrincipal.lblToneladasAcumuladas.configure(text="{0:.3f}".format(objIntercomunicacion["TONELADAS_ACUMULADAS"]))
            self.formularioPrincipal.lblKgValle.configure(text="{0:.3f}".format(objIntercomunicacion["KILOGRAMOS_VALLE_TOTAL"]))
            self.formularioPrincipal.lblLitrosPorTonelada.configure(text="{0:.2f}".format(objIntercomunicacion["LITROS_POR_TONELADA"]))
            self.formularioPrincipal.lblToneladasHoras.configure(text="{0:.2f}".format(objIntercomunicacion["TONELADAS_POR_HORA"]))
            self.formularioPrincipal.lblTiempoLlenadoJeringa.configure(text="{0:.2f}".format(objIntercomunicacion["TIEMPO_LLENADO_JERINGA"]))
            self.formularioPrincipal.lblVelocidadVariador.configure(text="{0:.2f}".format(objIntercomunicacion["VELOCIDAD_VARIADOR"]))
            self.formularioPrincipal.lblUltimaLecturaBaseDatos.configure(text="{}".format(objIntercomunicacion["UPDATED_AT"]))
            self.formularioPrincipal.lblImagenesPorSegundo.configure(text="{0:.2f}".format(objIntercomunicacion["TIEMPO_CICLO"]))
            self.formularioPrincipal.lblMililitrosPorMinuto.configure(text="{}".format(objIntercomunicacion["MILILITROS_POR_MINUTO"]))


