import threading
import time
import cv2
from dosificador.shared.Intercomunicador import Intercomunicador
from dosificador.backend.lib.adquisicion.SensorValle import SensorValle
from dosificador.shared.Configuracion import Configuracion
from dosificador.shared.RaspberryGPIO import RaspberryGPIO
from dosificador.backend.lib.actuacion.Bomba import Bomba

class FormParametrosUpdater(threading.Thread):
    def __init__(self,gui):
        
        self.formularioParametros = gui
        self._intercomunicador = Intercomunicador()
        self._camaraSeleccionada = None
        Configuracion()
        RaspberryGPIO() 
        SensorValle()
        self.escribir=Bomba()
        threading.Thread.__init__(self)
        self.start()
        self.actualizarDatosCentral()

    def run(self):
        loopActivo = True
        while(loopActivo):
            
            Configuracion.loadConfiguracionFromDatabase()
            
            if(self.formularioParametros.vecesPrecionadobotonPulsosrolos == 0 ):
                self.leerCamaraEnTiempoReal()
                
            if(self.formularioParametros.vecesPrecionadobotonPulsosrolos == 1 ):
                self.leerCamaraPorPulsoCinta()
            if(self.formularioParametros.vecesPrecionadobotonPulsosrolos == 2 ):    
                self.formularioParametros.vecesPrecionadobotonPulsosrolos = 0
            if(self.formularioParametros.bombaVelocidadmaximaVariador== 1):
                self.escribir.velocidadMaximavariador()                
            
            if(self.formularioParametros.bombaVelocidadmaximaVariador== 2):
                self.escribir.pararVariador()
                self.formularioParametros.bombaVelocidadmaximaVariador = 0
   
    def asignarCamaraSeleccionada(self):
        self.actualizarComponentesVisualesBomba()
        
        if (self.formularioParametros.cbCamaras.get()== "UNO"):
            self._camaraSeleccionada = self.formularioParametros._camaraSeleccionadauna
        if (self.formularioParametros.cbCamaras.get()== "DOS"):
            self._camaraSeleccionada = self.formularioParametros._camaraSeleccionadados
        if (self.formularioParametros.cbCamaras.get()== "TRES"):
            self._camaraSeleccionada = self.formularioParametros._camaraSeleccionadatres
        if (self.formularioParametros.cbCamaras.get()== "CUATRO"):
            self._camaraSeleccionada = self.formularioParametros._camaraSeleccionadacuatro
        
         
        if(self._camaraSeleccionada != None):
            self._camaraSeleccionada.loadConfiguracionCamara("camara_"+self.formularioParametros.cbCamaras.get().lower())
            
            return True
        else:
            return False
                
    def mostrarFotoTomadaEnPantalla(self):
        if(self.formularioParametros.vecesPrecionadobotonPulsosrolos == 0 ):
            fotoCamara = self._camaraSeleccionada.getImagenFromCamaramascara()
        if(self.formularioParametros.vecesPrecionadobotonPulsosrolos == 1 ):
            fotoCamara = self._camaraSeleccionada.getImagenFromCamarareal()
        else:
            fotoCamara = self._camaraSeleccionada.getImagenFromCamaramascara()
        winname="camara"
        cv2.namedWindow(winname)
        cv2.moveWindow(winname,1100,77)
        cv2.imshow(winname,fotoCamara)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            cv2.destroyAllWindows()      

    def leerCamaraEnTiempoReal(self):
        time.sleep(0.005)
        if(self.asignarCamaraSeleccionada() == True):
            self.actualizarComponentesVisuales()
            self.mostrarFotoTomadaEnPantalla()
            
            
    def leerCamaraPorPulsoCinta(self):
        if(SensorValle.debeTomarFotoCalibracion()==True and self.asignarCamaraSeleccionada() == True):
            self.actualizarComponentesVisuales()
            self.mostrarFotoTomadaEnPantalla()
    
    def recargarConfiguracionCamara(self):
        self._camaraSeleccionada.loadConfiguracionCamara("camara_"+self.formularioParametros.cbCamaras.get().lower())
    
    def actualizarDatosCentral(self):
        valorConfigDB = str (Configuracion.getValor('servidor_central'))
        self.formularioParametros.EntradaIpCentral.insert(0,valorConfigDB)
        valorConfigDB = str (Configuracion.getValor('servidor_central_puerto'))
        self.formularioParametros.EntradaNumeroPuerto.insert(0,valorConfigDB)
        valorConfigDB = str (Configuracion.getValor('dosificador_id'))
        self.formularioParametros.EntradaNumeroDosificador.insert(0,valorConfigDB)

    def actualizarComponentesVisuales(self):
        
        valorConfigDB = int(Configuracion.getValor("camara."+self._camaraSeleccionada.getNombreCamara()+".punto_uno_x"))
        self.formularioParametros.Scale1.set(valorConfigDB)
        
        valorConfigDB = int(Configuracion.getValor("camara."+self._camaraSeleccionada.getNombreCamara()+".punto_dos_x"))
        self.formularioParametros.Scale2.set(valorConfigDB)
        
        valorConfigDB = int(Configuracion.getValor("camara."+self._camaraSeleccionada.getNombreCamara()+".punto_uno_y"))
        self.formularioParametros.Scale3.set(valorConfigDB)
        
        valorConfigDB = int(Configuracion.getValor("camara."+self._camaraSeleccionada.getNombreCamara()+".punto_dos_y"))
        self.formularioParametros.Scale4.set(valorConfigDB)
        
        valorConfigDB = float(Configuracion.getValor("camara."+self._camaraSeleccionada.getNombreCamara()+".ajuste_uno_funcion_matricial"))
        self.formularioParametros.Scale5.set(valorConfigDB)
        
        valorConfigDB = float(Configuracion.getValor("camara."+self._camaraSeleccionada.getNombreCamara()+".ajuste_dos_funcion_matricial"))
        self.formularioParametros.Scale6.set(valorConfigDB)
        self.formularioParametros.lblValorDosMatrizNueva_2.configure(text="{:.3f}".format(valorConfigDB))

        valorConfigDB = float(Configuracion.getValor("camara."+self._camaraSeleccionada.getNombreCamara()+".verdes_bajos_color"))
        self.formularioParametros.Scale7.set(valorConfigDB)

        valorConfigDB = float(Configuracion.getValor("camara."+self._camaraSeleccionada.getNombreCamara()+".kilogramos_fruta"))
        self.formularioParametros.lblDisplayKgFrutaAnterior.configure(text="{:.3f}".format(valorConfigDB))
         
        valorConfigDB = float(self._camaraSeleccionada.calcularKgFruta())
        self.formularioParametros.ldlDisplayKGFruta.configure(text="{:.3f}".format(valorConfigDB)) 
        
        
    
    def actualizarComponentesVisualesBomba(self):

        valorConfigDB =float(Configuracion.getValor("estadisticas.mililitros_por_minuto_manual"))
        self.formularioParametros.lblDisplaymililitroMinuto.configure(text=valorConfigDB)

        valorConfigDB =float(Configuracion.getValor("estadisticas.tiempo_llenado_60_ml"))
        self.formularioParametros.lblDisplaytiempoAnteriorllenado.configure(text=valorConfigDB)

        valorConfigDB =float(Configuracion.getValor("estadistica.litros_por_tonelada"))
        self.formularioParametros.lblDisplayLitrosTonelada.configure(text=valorConfigDB)

        

        
